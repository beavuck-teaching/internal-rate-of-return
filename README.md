# Py_FinMath: group 17

[instructions.pdf](https://github.com/draialexis/Py_FinMath/files/8102300/Projet.Maths.fi.pdf) (in French)

[gp17_data.txt](https://docs.google.com/document/d/1fxsZYxGJz_l9gXe5SGrYZjONGLOFta0v5LgHHnpcHyI/edit?usp=drive_link)

[the_math.pdf](https://drive.google.com/file/d/1YBY_VcivoMF9MMmFQsHey6V8xDHnS387/view?usp=drive_link) (in French)

The bounds and error margins were hardcoded, and can be modified by hand on lines 3, 4 and 5
